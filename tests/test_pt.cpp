#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE pt_test

#include <boost/test/unit_test.hpp>

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>

#include "periodic_table.h"

using namespace std;

BOOST_AUTO_TEST_SUITE(PT_tests)

BOOST_AUTO_TEST_CASE(Test_One)
{
  const std::vector<element>  elems = global_periodic_table().get_elements();
  
  cout << "Total " << elems.size() << " elements in the table" << endl;
  
  cout << "Elements are:" << endl;        
  for(int i = 0; i < elems.size(); i++)
  {
    cout << elems[i].get_symbol() << " " << endl;
  }
}

BOOST_AUTO_TEST_SUITE_END()
