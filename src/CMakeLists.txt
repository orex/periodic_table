find_package(Boost 1.46 REQUIRED)

file(DOWNLOAD https://raw.github.com/orex/cpp_rsc/master/cmake/modules/cpp_resource.cmake
              ${CMAKE_BINARY_DIR}/cmake/modules/cpp_resource.cmake) 

              
set(CMAKE_MODULE_PATH ${CMAKE_BINARY_DIR}/cmake/modules)

include(cpp_resource)

find_resource_compiler()
add_resource(pt_rsc)
link_resource_file(pt_rsc FILE ${CMAKE_SOURCE_DIR}/data/PeriodicTable.xml VARIABLE table_xml TEXT)

get_property(RSC_CPP_FILE TARGET pt_rsc PROPERTY _AR_SRC_FILE)
get_property(RSC_H_DIR TARGET pt_rsc PROPERTY _AR_H_DIR)

include_directories(${RSC_H_DIR})

set(PT_LIB_SRC_FILES periodic_table.cpp ${RSC_CPP_FILE})

add_library(periodic-table-shared SHARED ${PT_LIB_SRC_FILES})
add_library(periodic-table-static STATIC ${PT_LIB_SRC_FILES})

set_target_properties(periodic-table-shared PROPERTIES OUTPUT_NAME periodic-table)
set_target_properties(periodic-table-static PROPERTIES OUTPUT_NAME periodic-table)

add_dependencies(periodic-table-shared pt_rsc)
add_dependencies(periodic-table-static pt_rsc)

install(TARGETS periodic-table-shared LIBRARY DESTINATION lib)